var path = require ('path');

module.exports = {
  devtool: 'cheap-source-maps',
  module: {
    loaders: [{
      test: /\.js$/,
      include: [
        path.resolve(__dirname, 'src')
      ],
      loader: 'babel-loader'
    }, {
      test: /\.css$/, loader: 'style-loader!css-loader'
    }]
  },
  entry: {
    app: ["./src/index.js"]
  },
  output: {
    path: path.join(__dirname, 'dist'),
    publicPath: 'dist',
    filename: 'bundle.js'
  }
};