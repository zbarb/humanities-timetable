let idx = 0;

export class TopicClass {
  constructor(data) {
    let { payload } = data;
    this.payload = payload;
    this.id = payload['topic code'] + idx++; // Some topics are duplicated
    this.topicName = payload['topic code'].trim();
    this.name = payload['topic name'].trim();
    this.campus = payload.campus.trim();
    this.sem = payload.sem.trim();
    this.room = data.room.trim();
    this.roomType = data.roomType.trim();
    this.firstName = payload.firstname.trim();
    this.surname = payload.surname.trim();
    this.start = data.start;
    this.end = data.end;
    this.duration = data.duration;
    this.dow = data.dow;
    this.next = data.next;
    this.plus = payload['plus/and/or'];
    this.core = payload.core.split(',').map(name => name.trim()).filter(name => !!name);
    this.plusAnd = payload['plus/and/or'].split(',').map(name => name.trim()).filter(name => !!name);
    this.title = `${this.topicName} (${this.room}): ${this.surname}, ${this.firstName}`;
    this.clashesWith = [];
    this.checked = [];
    this.clashing = [];
  }

  buildClashMap(topics) {
    topics.forEach(topic => {
      if(topic === this || ~topic.checked.indexOf(this)) return;
      this.checked.push(topic);
      if(intersect(this.core, topic.core).length > 0) {
        return this.clash(topic, 'core');
      }
      if(topic.firstName + topic.lastName === this.firstName + this.lastName) {
        return this.clash(topic, 'teacher');
      }
      if(this.room === topic.room) {
        return this.clash(topic, 'room');
      }
    });
  }

  isClashingWith(topic) {
    let idx = this.clashesWith.find(clash => clash.topic === topic);
    if(!idx) {
      return false;
    }

    if(!topic.start || !topic.end) {
      return false;
    }

    if(this.start.isBetween(topic.start, topic.end, null, '[]') ||
    this.end.isBetween(topic.start, topic.end, null, '[]')) {
      return true;
    }
    return false;
  }

  hasClashes(topics) {
    try {
      topics.forEach(topic => {
        if(this.isClashingWith(topic)) throw new Error('clash');
      });
      return false;
    } catch(ex) {
      return true;
    }
  }

  clash(topic, reason) {
    this.clashesWith.push({
      reason,
      topic: topic
    });
    topic.clashesWith.push({
      reason,
      topic: this
    });
  }
}

function intersect(a, b) {
  a = a.slice().sort();
  b = b.slice().sort();
  let result = [];
  while( a.length > 0 && b.length > 0 ) {  
     if (a[0] < b[0]) a.shift();
     else if (a[0] > b[0]) b.shift();
     else {
       result.push(a.shift());
       b.shift();
     }
  }

  return result;
}