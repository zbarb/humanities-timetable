export const TIME_FORMAT = 'HH:mm';
export const DAY_START = 8;
export const DAY_END = 18;

export const FILTERS = [{
  key: 'topic code',
  filter: 'topicName'
}, {
  key: 'surname',
  filter: 'surname'
}, {
  key: 'campus',
  filter: 'campus'
}, {
  key: 'sem',
  filter: 'sem'
}];