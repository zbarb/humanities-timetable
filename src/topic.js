import { TopicClass } from './topic-class';
import { TIME_FORMAT } from './constants';

export class Topic {
  constructor(payload, loader) {
    this.students = payload['number of students (max 20 per tutorial/workshop) unlimited for lectures'];
    this.payload = payload;
    this.loader = loader;
    try {
      this.rooms = payload.room.split(',').map(name => name.trim());
    } catch(ex) {
      this.rooms = [];
    }
  }

  get classes() {
    return this.rooms
      .map(room => {
        let idx = room.indexOf('(');
        let roomType = idx >= 0 ? room.slice(0, idx) : room;
        switch(roomType) {
          case 'lecture':
          case 'seminar':
            return [this.topicClass(room, roomType)];
          case 'lab':
          case 'tutorial':
            return this.limitedClass(20, room, roomType);
          default:
            return;
        }
      })
      .reduce((a, b) => a.concat(b), []);
  }

  limitedClass(maxStudents, room, type) {
    if(maxStudents <= 0 || !this.students || this.students < 0) {
      throw new Error('Infinite loop');
    }
    let students = 0;
    let rooms = [];
    while(students < this.students) {
      rooms.push(
        this.topicClass(room, type)
      );
      students += maxStudents;
    }
    return rooms;
  }

  topicClass(room, roomType) {
    return new TopicClass(Object.assign({}, {
      room,
      roomType,
      payload: this.payload,
      duration: this.hoursForRoom(roomType),
    }));
  }

  hoursForRoom(roomType) {
    switch(roomType) {
      case 'lecture':
      case 'tutorial':
        return 1;
      case 'lab':
        return 2;
      case 'seminar':
        return 3;
      default:
        return;
    }
  }
}