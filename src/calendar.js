import * as $ from 'jquery';
import * as moment from 'moment';
import 'fullcalendar';
import * as toCsv from 'json2csv';
require('fullcalendar/dist/fullcalendar.css');

import { DEFAULT_CALENDAR_SETTINGS } from './settings';
import { TIME_FORMAT } from './constants';

const OK = '#3a87ad';
const BAD = '#B80C09';

export class Calendar {
  constructor(topics) {
    this.filters = {};
    this.config = Object.assign({}, DEFAULT_CALENDAR_SETTINGS, {
      eventDrop: this.eventDrop.bind(this),
      eventMouseover: this.eventMouseover.bind(this),
      eventMouseout: () => $('#moreinfo').hide()
    });
    this.run(this.config);
    this.run('removeEventSources');
    this.run('addEventSource', topics.map(topic => Object.assign(topic, {topic})));
    this.events = this.run('clientEvents');
    this.events.forEach(event => event.topic.event = event);
    this.events.forEach(event => this.eventDrop(event));
    this.reflow();
  }
  get run() {
    return $('#calendar').fullCalendar.bind($('#calendar'));
  }
  filter(key, value) {
    this.filters[key] = value;
    this.reflow();
  }

  reflow() {
    this.events.forEach(item => {
      item.className = '';
      if(!Object.keys(this.filters).length) {
        return;
      }
      Object.keys(this.filters).forEach(key => {
        if(!this.filters[key]) {
          return;
        }
        if(Array.isArray(item.topic[key])) {
          this.filters[key].forEach(filt => {
            let willBreak = false;
            item.topic[key].forEach(el => {
              if(el === filt) {
                willBreak = true;
              }
            });
            if(willBreak) {
              return item.className = '';
            }
            item.className = 'hidden';
          });
        } else if(!~this.filters[key].indexOf(item.topic[key])) {
          item.className = 'hidden';
        }
      });
    });
    this.run('rerenderEvents');
  }

  eventDrop(event, rerender, reset) {
    if(reset) {
      event.backgroundColor = OK;
      event.clashing.forEach(clash => clash.event.backgroundColor = OK);
      event.clashing = [];
    }
    event.clashesWith.forEach(clash => {
      clash = clash.topic;
      let idx = clash.clashing.indexOf(event.topic);
      if(reset && idx >= 0) {
        clash.clashing.splice(idx, 1);
        if(clash.clashing.length === 0) {
          clash.event.backgroundColor = OK;
        }
      }
      if(event.start.isBetween(clash.event.start, clash.event.end, null, '[]') ||
        event.end.isBetween(clash.event.start, clash.event.end, null, '[]')) {
          clash.event.backgroundColor = BAD;
          event.backgroundColor = BAD;
          event.topic.clashing.push(clash);
          clash.clashing.push(event.topic);
      }
    });

    if(rerender) {
      this.run('rerenderEvents');
    }
  }

  eventMouseover(event, jsevent, view) {
    let {
      name,
      sem,
      surname,
      firstName,
      topicName,
      title,
      core,
      plus,
      room,
    } = event;
    $('#moreinfo').html([
      `<strong>Class Info</strong>`,
      `Name: ${name}`,
      `Room: ${room}`,
      `Semester: ${sem}`,
      `FirstName: ${firstName}`,
      `Surname: ${surname}`,
      `Topic: ${topicName}`,
      `Title: ${title}`,
      `Core: ${core}`,
      `Plus/And/Or: ${plus}`
    ].join('<br/>'));
    $('#moreinfo').show();
    $('#moreinfo').css({
      position: 'fixed',
      left: `${jsevent.pageX - window.scrollX - 90}px`,
      top: `${jsevent.pageY - window.scrollY}px`
    });
  }

  save() {
    let data = this.events.map(event => ({
        topic: event.topic,
        room: event.room,
        start: event.start.format('ddd HH:mm a'),
        end: event.end.format('ddd HH:mm a')
      }));
    let fields = ['topic','room','start','end'];
    let csv = toCsv({ data, fields });
    window.open(encodeURI(`data:text/csv;charset=utf-8,${csv}`));
  }
}