export const DEFAULT_CALENDAR_SETTINGS = {
  defaultView: 'agendaWeek',
  allDaySlot: false,
  businessHours: {
    start: '07:00',
    end: '20:00'
  },
  header: {
    // left: '',
    // center: '',
    right: ''
  },
  minTime: '05:00',
  maxTime: '22:00',
  editable: true,
  startEditable: true,
  eventStartEditable: true,
  displayEventTime: true,
  events: [
  // {
  //   title: 'All Day Event',
  //   // start: '10:00',
  //   date: '2017-05-10 10:00'
  // }
  ]
}