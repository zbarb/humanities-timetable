// Required by calendar
import * as $ from 'jquery';
import * as moment from 'moment';
import 'fullcalendar';
require('fullcalendar/dist/fullcalendar.css');

import { FileLoader } from './file-loader';

(() => {
  $('#file').change(() => {
    let input = $('#file')[0];
    if (!input.files.length) {
      return alert('Please select a file first');
    }
    let fileLoader = new FileLoader(input);
    $('.controls').show();
    $('.input').hide();
  });
  $('#toggleFilters').click(() => $('.toggles').toggle());
})();