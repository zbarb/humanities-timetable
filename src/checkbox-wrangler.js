import * as $ from 'jquery';

export class CheckboxWrangler {
  constructor(items, key, filterFn) {
    this.items = items;

    let cont = $(document.createElement('div'));
    let header = $(document.createElement('p'));
    let clear = $(document.createElement('button'));
    let select = $(document.createElement('select'));


    clear.click(() => select.val(false) && filterFn(key, false));
    select.change(() => filterFn(key, select.val()));

    clear.html('Clear');
    header.html(key.slice(0, 1).toUpperCase() + key.slice(1));

    cont.addClass('filter');
    select.attr('multiple', true);

    items = items.map(a => a.trim());
    items = items.sort();

    items.forEach(item => {
      let option = $(document.createElement('option'));
      option.attr('value', item);
      option.html(item);
      select.append(option);
    });

    cont.append(clear);
    cont.append(header);
    cont.append(select);

    $('.toggles').append(cont);
  }
}