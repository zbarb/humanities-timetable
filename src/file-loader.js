import { DEFAULT_CALENDAR_SETTINGS } from './settings';
import { Calendar } from './calendar';
import { Topic } from './topic';
import { CheckboxWrangler } from './checkbox-wrangler';
import getUniques from './uniques';
import * as moment from 'moment';
import * as $ from 'jquery';

const Papa = require('papaparse');

import { FILTERS } from './constants';
import {Sorter} from './sorter';

export class FileLoader {
  constructor(input) {
    Papa.parse(input.files[0], {
      header: true,
      complete: this.ready.bind(this)
    });
  }

  ready(result) {
    let rows = result.data;
    let allEvents = {};
    let topics = rows.map(row => new Topic(row, this));
    let classes = topics
      .map(topic => topic.classes)
      .reduce((p, n) => p.concat(n), [])
      .filter(val => !!val);

    classes.forEach(classI => classI.buildClashMap(classes));

    let sorter = new Sorter(classes);

    this.calendar = new Calendar(sorter.sort());
    $('#save').click(() => this.calendar.save());

    FILTERS.forEach(filter => 
      new CheckboxWrangler(getUniques(rows, filter.key), filter.filter, this.calendar.filter.bind(this.calendar))
    );

    let degrees = {};
    rows.forEach(row => {
      if(!row.core) {
        return;
      }
      row.core.split(',').map(val => val.trim()).filter(val => !!val).forEach(deg => degrees[deg] = true)
    });
    new CheckboxWrangler(Object.keys(degrees), 'core', this.calendar.filter.bind(this.calendar));
    let rooms = {};
    rows.forEach(row => {
      if(!row.room) {
        return;
      }
      row.room.split(',').map(val => val.trim()).filter(val => !!val).forEach(rm => rooms[rm] = true)
    });
    new CheckboxWrangler(Object.keys(rooms), 'room', this.calendar.filter.bind(this.calendar));
  }
}