export default function(rows, key) {
  let uniques = rows.reduce((total, row) => Object.assign(total, {[row[key]]: row}), {});
  return Object.keys(uniques);
}