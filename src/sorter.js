import { TIME_FORMAT, DAY_START, DAY_END } from './constants';
import * as moment from 'moment';

export class Sorter {
  constructor(topics) {
    this.topics = topics;
    this.currentTime = moment().day(1).hour(DAY_START).minute(0);
  }

  startTime() {
    return this.currentTime.clone();
  }

  endTime(hrs) {
    let time = this.currentTime.clone().add(hrs, 'hour').subtract(10, 'minutes');
    this.currentTime = this.currentTime.clone().add(hrs, 'hour');
    if (this.currentTime.hours() >= DAY_END) {
      this.currentTime = this.currentTime.add(1, 'days').hour(DAY_START);
    }
    if (this.currentTime.day() > 5) {
      this.currentTime.day(1);
    }
    return time;
  }

  sort() {
    this.sortLectureFirst();
    this.sortName();
    this.assignTimes();

    this.assignHours();
    return this.topics;
  }

  sortName() {
    this.topics = this.topics.sort((a, b) => 
      a.topicName < b.topicName ? -1
      : a.topicName === b.topicName ? 0
      : 1);
  }

  sortLectureFirst() {
    this.topics = this.topics.sort((a, b) =>
     a.topicName !== b.topicName ? 0 :
     a.room === 'lecture' && b.room !== 'lecture' ? -1 :
     b.room === 'lecture' && a.room !== 'lecture' ? 1 : 0);
  }

  assignTimes() {
    this.topics.forEach(topic => {
      let INFLP = 100;
      let i = 0;
      do {
        this.assignTime(topic);
      } while(++i && i < INFLP && topic.hasClashes(this.topics));
    });
  }

  assignTime(topic) {
    topic.start = this.startTime();
    topic.end = this.endTime(topic.duration);
  }

  assignHours() {
    this.topics.forEach(topic => {
      topic.dow = [topic.start.day()];
      topic.start = topic.start.format(TIME_FORMAT);
      topic.end = topic.end.format(TIME_FORMAT);
    });
  }
}

class TopicBlock {
  constructor(topics) {
    this.topics = topics.sort((a, b) => a.room === 'lecture' && b.room !== 'lecture' ? -1 :
     b.room === 'lecture' && a.room !== 'lecture' ? 1 : 0);
    this.duration = this.topics.reduce((a, b) => a + b.duration, 0);
  }

  get end() {
    return this.topics[0].start.clone().add(this.duration);
  }

  get start() {
    return this.topics[0].start.clone();
  }

  set start(time) {
    let currentTime = time.clone();
    this.topics.forEach(topic => {
      topic.start = currentTime.clone();
      topic.end = currentTime.clone().add(topic.duration, 'hours').subtract(10, 'minutes');
      currentTime = currentTime.add(topic.duration, 'hours');
    });
  }

  clashesWith(topicGroup) {
    try {
      this.topics.forEach(topic => {
        topicGroup.forEach(clash => {
          if(topic.clashesWith(clash)) {
            throw new Error('clash');
          }
        });
      });
    } catch(err) {
      return true;
    }
    return false;
  }
}