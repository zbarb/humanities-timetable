import getUniques from './uniques';

export class Faculty {
  constructor(payload) {
    this.topics = getUniques(payload, 'topic code');
  }
}